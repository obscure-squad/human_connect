var router = require('express').Router()

router.use(require('express').static(__dirname + '/../assets'))

router.use(require('express').static(__dirname + '/../templates'))

router.get('/',function(req,res){
	res.sendfile('layouts/app.html');
})

module.exports = router 