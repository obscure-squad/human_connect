angular.module('app').controller('PostsCtrl',function($scope,PostsSvc){

	$scope.addPost = function(){

		if($scope.postBody){
			
			PostsSvc.create({
					username : $scope.postName,
					body : $scope.postBody
			}).success(function(post){
				
				$scope.posts.unshift(post)
				$scope.postBody = null
				$scope.postName = null
			}).error(function(err){
				console.log(err);
			})
		}
	}

	PostsSvc.fetch().success(function(posts){
		$scope.posts = posts;
	})

})