var Post = require('../../models/post')
var router = require('express').Router()

router.post('/api/posts',function(req,res,next){
	console.log(req.body);
	var post = new Post({
		username : req.body.username,
		body : req.body.body
	})
	post.save(function(err,post){
		
		if(err) { return next(err) }

		res.json(201,post)
	})
})


router.get('/api/posts',function(req,res){

	Post.find().sort('-date').exec(function(err,posts){
		if(err) return next(err) 

		res.json(posts)
	})
})


module.exports = router

