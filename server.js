var express = require('express')
var bodyParser = require('body-parser')

/* JWT **/
var jwt = require('jwt-simple')
var _ = require('lodash') 
var bcrypt = require('bcrypt')

var Post = require('./models/post')
var User = require('./models/user')
var app = express()

app.use(bodyParser.json())
var users = [
		{username:'sazalxxxx',password:'$2a$10$9e7GHuLx0e5o6Oo6dKoQueC39Iog5MuKLHo8zIdkZv3b1Ux03r1b2'},
		{username:'prosenxxx',password:'$2a$10$9e7GHuLx0e5o6Oo6dKoQueC39Iog5MuKLHo8zIdkZv3b1Ux03r1b2'}
	]
var secrectKey = 'supersecretkey'

function findUserByUsername(username){
	//console.log(username);
	return _.find(users,{ username: username })
} 
function validateUser(user,password,cb){
	//return user.password = password 
	return bcrypt.compare(password,user.password,cb)
}

app.use(require('./controllers/api/posts'))
app.use(require('./controllers/static'))

/* JWT */
app.post('/session',function(req,res){

	var username = req.body.username
	var password  = req.body.password
	console.log(password)
	User.findOne({username:username}).select('password')
		.exec(function(err,user){
			if(err) { return next(err) }

			if(!user) { return res.send(401) }

				bcrypt.compare(password,user.password,function(err,valid){
					if(err) { return next(err) }

					if( ! valid){ return res.send(401) }

					var token = jwt.encode({username:user.username},secrectKey)

					res.json(token)
				})
		})
	
})
app.post('/user',function(req,res,next){

	var user = new User({username:req.body.username})

	bcrypt.hash(req.body.password,10,function(err,hash){
		user.password = hash
		user.save(function(err,user){
			if(err) { throw next(err) }

			res.send(201) // ok
		})
	})
})

app.get('/user',function(req,res){
	console.log('ok',req.headers['x-auth'])
	var token = req.headers['x-auth']
	var auth = jwt.decode(token,secrectKey)
	User.findOne({username:auth.username},function(err,user){
		res.json(user)
	})
	//res.json(user)
})

app.listen(4000,function(){
	console.log('Server listing on',4000)
})
